#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 20 14:55:00 2020

@author: jehamann
"""

from chargecurve import ChargingCurve, DriveCurve, AutoCharge, AutoDrive
import pandas as pd
import numpy as np
from databases import load_base2_ftp
from functions import get_peaks, collect_charges, collect_drives
import datetime

#########################################
#Collect data 

#list with available OBD ecu data
ecu_list = [
            'ecu_bcddc2ceb734',
            'ecu_bcddc2d07024',
            'ecu_bcddc2d0718c',
            'ecu_bcddc2d03734',
            'ecu_bcddc2ce6344',
            'ecu_bcddc2ce654c',
            'ecu_bcddc2d07034',
            ]

table = pd.DataFrame()
for ecu in ecu_list:
    data = load_base2_ftp(ecu).iloc[-1000:]
    data['ecu'] = [ecu for n in range(len(data))]
    table = pd.concat([table,data])

table = table.groupby('ecu').agg(
        model = ('model', 'last'),
        VIN = ('VIN', 'last'),
        latitude = ('latitude', 'mean'),
        longitude = ('longitude', 'mean'),
        speed = ('speed', 'mean'),
        altitude = ('altitude', 'mean'),
        odometer = ('odometer', 'mean'),
        hvBatteryVoltage = ('hvBatteryVoltage', 'mean'),
        hvBatteryCurrent = ('hvBatteryCurrent', 'mean'),
        hvBatteryTemp = ('hvBatteryTemp', 'mean'),
        hvSocActualBms = ('hvSocActualBms', 'mean'),
        hvSocActualDisplay = ('hvSocActualDisplay', 'mean'),
        hvAvailableChargePower = ('hvAvailableChargePower', 'mean'),
        hvAvailableDischargePower = ('hvAvailableDischargePower', 'mean'),
#        rpmEmotor = ('rpmEmotor', 'mean'),
#        timestamp = ('timestamp', 'last'),
#        eCUver = ('eCUver', 'last'),
        )
    
print(table)

