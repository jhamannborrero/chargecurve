import pandas as pd
from ftplib import FTP
import matplotlib.pyplot as plt
import datetime, time, os 
import numpy as np

##################################
# functions to access the base2 data
##################################

#data_path = '/home/jehamann/eClever/databases/base2/base2data/'
#data_path = '../data/'

#function to load base2 data files depeding on their eCU version
def get_files(ecu):
    """
    Cheks if the data file is older than 10 minutes and downloads it from the server.
    Downloads the csv file in the data_path folder.
    ecu (string): ecu number (file name without extension) 
    """
    
    global data_path

    # check if the file is older than 10 mins, if yes, reload
    now = time.time()
    
    base2 = FTP(host='eclever.base2.de', user='f01251de', passwd='eculogs1234')
#    data_path = '/home/jehamann/eClever/databases/base2/base2data/'
    data_path = '../data/'
    file_name =  ecu + '.csv'

    try:
        
        mod = os.path.getmtime(data_path + ecu + '.csv')
    
    except:
        
        temp_file = open(data_path + file_name, 'wb')
        base2.retrbinary('RETR ' + file_name, temp_file.write)#, 1024) # Enter the filename to download
        temp_file.close() # Close the local file you had opened for downloading/storing its content
        base2.quit() # Terminate the FTP connection
        
    else:
        
        if (now - mod) > 10*60:
    
            temp_file = open(data_path + file_name, 'wb')
            base2.retrbinary('RETR ' + file_name, temp_file.write)#, 1024) # Enter the filename to download
            temp_file.close() # Close the local file you had opened for downloading/storing its content
            base2.quit() # Terminate the FTP connection
        
        else:
            pass

def load_ecu_ver(ecu, ver):
    """
    Function to extract data from the csv files depending on the eCUver.
    Returns a pandas DataFrame with data for each eCUver
    ecu = csv file as string with the ecu data to be loaded.
    ver = eCU version number (int)
    eg: test = load_base2('ecu_bcddc2d0718c', 208)
    """
  
    # define column names for given eCU version
    if ver == 205:
        col_names = ["id_","id","ecpPlugTemp","BluetoothConnection","model",
                     "VIN","latitude","longitude","speed","altitude","odometer",
                     "motionless_for","lvBatteryVoltage","insideActualTemp",
                     "outsideAirTemp","hvBatteryVoltage","hvBatteryCurrent",
                     "hvBatteryTemp","hvSocActualBms","hvSocActualDisplay",
                     "hvStateOfHealth","hvAvailableChargePower","hvAvailableDischargePower",
                     "rpmEmotor","timestamp","eCUtime","milliseconds_without_sufficient_voltage",
                     "ecuTemp","celltemp","ecu12v","vsys","pcbtemp",
                     "FreeHeapSize","MinimumEverFreeHeapSize","rssi",
                     "mobilesystemmode","CommReInitializations","buffer_remain",
                     "log_filenum_highest","eCUver","eCUcompiled","ubx_stat",
                     "ubx_lat","ubx_lon","ubx_speed","ubx_alt","ubx_time"
                     ]
    
        drop_cols = ["id_","id","ecpPlugTemp","BluetoothConnection",
                     "motionless_for","lvBatteryVoltage","insideActualTemp",
                     "outsideAirTemp","hvStateOfHealth","eCUtime","milliseconds_without_sufficient_voltage",
                     "ecuTemp","celltemp","ecu12v","vsys","pcbtemp",
                     "FreeHeapSize","MinimumEverFreeHeapSize","rssi",
                     "mobilesystemmode","CommReInitializations","buffer_remain",
                     "log_filenum_highest","eCUcompiled","ubx_stat",
                     "ubx_lat","ubx_lon","ubx_speed","ubx_alt","ubx_time"
                     ]
    elif ver == 208 or ver == 209:
        
        col_names = ["id_","id","ecpPlugTemp","BluetoothConnection","model",
                     "VIN","latitude","longitude","speed","altitude","odometer",
                     "motionless_for","lvBatteryVoltage","insideActualTemp",
                     "outsideAirTemp","hvBatteryVoltage","hvBatteryCurrent",
                     "hvBatteryTemp","hvSocActualBms","hvSocActualDisplay",
                     "hvStateOfHealth","hvAvailableChargePower","hvAvailableDischargePower",
                     "rpmEmotor","timestamp","eCUtime","milliseconds_without_sufficient_voltage",
                     "celltemp","ecu12v","vsys","ecupcbtemp","FreeHeapSize","MinimumEverFreeHeapSize",
                     "rssi","mobilesystemmode","CommReInitializations","buffer_remain",
                     "log_filenum_highest","reset_reason","eCUver","eCUcompiled",
                     "ubx_stat","quec_lat","quec_lon","quec_speed","quec_alt",
                     "quec_time",'a'
                    ]

        drop_cols = ["id_","id","ecpPlugTemp","BluetoothConnection",
                     "motionless_for","lvBatteryVoltage","insideActualTemp",
                     "outsideAirTemp","hvStateOfHealth","eCUtime",
                     "milliseconds_without_sufficient_voltage","celltemp",
                     "ecu12v","vsys","ecupcbtemp","FreeHeapSize",
                     "MinimumEverFreeHeapSize","rssi","mobilesystemmode",
                     "CommReInitializations","buffer_remain","log_filenum_highest",
                     "reset_reason","eCUcompiled","ubx_stat","quec_lat",
                     "quec_lon","quec_speed","quec_alt","quec_time",'a'
                    ]
    
    elif ver == 203:

        col_names = ["id_","id","ecpPlugTemp","BluetoothConnection","model",
                     "VIN","latitude","longitude","speed","altitude","odometer",
                     "lvBatteryVoltage","insideActualTemp",
                     "outsideAirTemp","hvBatteryVoltage","hvBatteryCurrent",
                     "hvBatteryTemp","hvSocActualBms","hvSocActualDisplay",
                     "hvStateOfHealth","hvAvailableChargePower","hvAvailableDischargePower",
                     "rpmEmotor","timestamp","eCUtime","milliseconds_without_sufficient_voltage",
                     "ecuTemp","celltemp","ecu12v","vsys","pcbtemp",
                     "FreeHeapSize","MinimumEverFreeHeapSize","rssi",
                     "mobilesystemmode","CommReInitializations","buffer_remain",
                     "log_filenum_highest","eCUver","eCUcompiled"
                     ]
    
        drop_cols = ["id_","id","ecpPlugTemp","BluetoothConnection",
                     "lvBatteryVoltage","insideActualTemp",
                     "outsideAirTemp","hvStateOfHealth","eCUtime","milliseconds_without_sufficient_voltage",
                     "ecuTemp","celltemp","ecu12v","vsys","pcbtemp",
                     "FreeHeapSize","MinimumEverFreeHeapSize","rssi",
                     "mobilesystemmode","CommReInitializations","buffer_remain",
                     "log_filenum_highest","eCUcompiled"
                     ]
    
    elif ver == 204:

        col_names = ["id_","id","ecpPlugTemp","BluetoothConnection","model",
                     "VIN","latitude","longitude","speed","altitude","odometer",
                     "lvBatteryVoltage","insideActualTemp",
                     "outsideAirTemp","hvBatteryVoltage","hvBatteryCurrent",
                     "hvBatteryTemp","hvSocActualBms","hvSocActualDisplay",
                     "hvStateOfHealth","hvAvailableChargePower","hvAvailableDischargePower",
                     "rpmEmotor","timestamp","eCUtime","milliseconds_without_sufficient_voltage",
                     "ecuTemp","celltemp","ecu12v","vsys","pcbtemp",
                     "FreeHeapSize","MinimumEverFreeHeapSize","rssi",
                     "mobilesystemmode","CommReInitializations","buffer_remain",
                     "log_filenum_highest","eCUver","eCUcompiled","ubx_stat",
                     "ubx_lat","ubx_lon","ubx_speed","ubx_alt","ubx_time"
                     ]
    
        drop_cols = ["id_","id","ecpPlugTemp","BluetoothConnection",
                     "lvBatteryVoltage","insideActualTemp",
                     "outsideAirTemp","hvStateOfHealth","eCUtime","milliseconds_without_sufficient_voltage",
                     "ecuTemp","celltemp","ecu12v","vsys","pcbtemp",
                     "FreeHeapSize","MinimumEverFreeHeapSize","rssi",
                     "mobilesystemmode","CommReInitializations","buffer_remain",
                     "log_filenum_highest","eCUcompiled","ubx_stat",
                     "ubx_lat","ubx_lon","ubx_speed","ubx_alt","ubx_time"
                     ]

    file_name = data_path + ecu + '.csv'    
    ecu = pd.read_csv(file_name, sep=';', header = None, names=col_names, index_col =False)
    ecu = ecu.drop(columns=drop_cols)
    ecu['eCUver'] = ecu['eCUver'].astype(str)
    ecu = ecu[(ecu.model != 'CAR_UNDETERMINED') & (ecu.eCUver == str(ver))].iloc[:-2]

    return ecu


def check_time(x):
    """
    function to check time stamps to have the proper format
    """
    try:
        new_x = pd.to_datetime(x)
    except:
        new_x = np.nan
    return new_x

def load_base2_ftp(ecu):
    """
    Load all data from the csv file and returns a pandas DataFrame with data from all eCUver
    """
    
    #load files from ftp server
    get_files(ecu)
    
    versions = [203,204,205,208,209]
    
    ecu_data = pd.DataFrame()
    # load data from file considering eCUver
    for ver in  versions:
        try:
            ver_data = load_ecu_ver(ecu, ver)
        except:
#            print('{} error: no data with eCUver {}'.format(ecu,ver))
            ver_data = pd.DataFrame()
        finally:
            pass

        ecu_data = ecu_data.append(ver_data)
    
    ecu_data['rpmEmotor'] = ecu_data['rpmEmotor'].astype(float)
    ecu_data['timestamp'] = ecu_data['timestamp'].apply(check_time)
    ecu_data = ecu_data.drop_duplicates(subset='timestamp')
    ecu_data['timestamp'] = ecu_data['timestamp'].dt.tz_convert(None)
    ecu_data = ecu_data.dropna(subset=['timestamp'])    
    
    #file_name= data_path + ecu + '.csv'
#    os.remove(file_name)

    return ecu_data
        
def base2_plot(ecu, Y1, Y2, start_date, end_date, second_plot = True):#, date_i, date_f):
    """
    Function to plot interactively live data from the OBD inside autos
    """
    
    df = load_base2_ftp(ecu)
   
    # mask to select data within the dates indicated
    mask = (df.timestamp > pd.to_datetime(start_date)) & (df.timestamp <  pd.to_datetime(end_date))
    
    fig, ax1 =plt.subplots()
    # left Y
    ax1.clear()
    ax1.plot(df[mask]['timestamp'],df[mask][Y1],'.',c='b', alpha = 0.3)
    ax1.set_ylabel('Y1: ' + Y1, color='blue')
    ax1.set_xlabel('timestamp')
#    ax1.set_navigate(False)
    ax1.tick_params(axis='x',labelrotation=30)
    ax1.grid(True, axis='x')
    
    if second_plot:
        
        #right Y
        ax2 = ax1.twinx()
        ax2.clear()
        ax2.plot(df[mask]['timestamp'],df[mask][Y2],'.',c='g', alpha = 0.3)
        ax2.set_ylabel('Y2: ' + Y2, color='green')

    plt.title(df.model.unique()[-1])
    plt.tight_layout()
    

#define initial time spam to show in the plot (current day)
sd = datetime.date.today()
ed =  sd + datetime.timedelta(days=1)
#ed =  sd.replace(day = sd.day + 1)

#list with available OBD ecu data
ecu_list = [
            'ecu_bcddc2ceb734',
            'ecu_bcddc2d07024',
            'ecu_bcddc2d0718c',
            'ecu_bcddc2d03734',
            'ecu_bcddc2ce6344',
            ]

# columns to select for plotting
cols = [
        'hvSocActualDisplay', 
        'hvSocActualBms', 
        'hvBatteryVoltage', 
        'hvBatteryCurrent', 
        'hvBatteryTemp',
        'hvAvailableChargePower',
        'hvAvailableDischargePower', 
        'latitude',
        'longitude',
        'altitude',
        'speed',
        'odometer', 
        'rpmEmotor', 
        'timestamp', 
        'eCUver',
        'model', 
#        'VIN',
        ]

############################################
## Funtions to access data from PosgreSQL DB
############################################

import psycopg2

def query_station(lat, lon):
    """
    Query the ChargeStations from 'stations' DB based on the given latitude and longitude information.
    Returns a pandas DataFrame with charge point information located at the given lat, lon location.
    lat (float): latitude
    lon (float): longitude
    """
    
    #PostgreSQL connection (Charge Station data)
    pg_host = "10.19.160.198"
    pg_port = "5432"
    pg_user = "stations-api"
    pg_password = "Zj9XkLMu8BCJ3_sK7WGYqhD"
    database = "stations"

    con = psycopg2.connect(database=database, user=pg_user, password=pg_password, host=pg_host, port=pg_port)
    cur = con.cursor()

    delta = 6e-4   # +/-delta range for the latitude/longitude

    # query the stations DB for the location of the charge station at charging time
    query = "SELECT cs.longitude, cs.latitude, cs.\"addressStreet\", cs.id AS chargestation_id, cp.id AS chargepoint_id, cp.kw, cp.ampere, cp.volt, cp.type "\
    "FROM \"chargepoints\" AS cp LEFT JOIN \"chargegroups\" AS cg ON cg.id = cp.chargegroup_id "\
    "LEFT JOIN \"chargestations\" AS cs ON cs.id = cg.chargestation_id "\
    "WHERE latitude BETWEEN {} AND {} AND longitude BETWEEN {} AND {};".format(lat-delta, lat+delta,lon-delta, lon+delta)
    cur.execute(query)
    cs = pd.read_sql_query(query,con)
    con.close()
    
    return cs

def query_vehicle(model):
    """
    Query the vehicle by model from 'vehicleTypes' DB.
    
    Parameters:
        model = string with the vehicle model. Eg., 'IONIQ Elektro'
    
    Returns:
        A pandas DataFrame with vehicle informationTopspeed:
            top speed, empty weight, drag coefficient, frontal area, netto kWh of the batttery, motor Torque, Motor Power
    """
    
    #PostgreSQL connection (Charge Station data)
    pg_host = "10.19.160.198"
    pg_port = "5432"
    pg_user = "stations-api"
    pg_password = "Zj9XkLMu8BCJ3_sK7WGYqhD"
    database = "stations"

    con = psycopg2.connect(database=database, user=pg_user, password=pg_password, host=pg_host, port=pg_port)
    cur = con.cursor()

    # query the stations DB for the location of the charge station at charging time
    query = "SELECT model, \"topSpeed\", \"emptyWeight\", \"dragFactor\", " \
    "\"frontFace\" as area, \"nettoKwh\", \"bruttoKwh\", etorque, \"endEnginePower\" "\
    "FROM  \"vehicletypes\" WHERE model = '{}'; ".format(model)
    cur.execute(query)
    cs = pd.read_sql_query(query,con)
    con.close()
    
    return cs
