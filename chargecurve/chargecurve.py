#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 30 13:59:03 2020

@author: jehamann
"""
import numpy as np
import matplotlib.pyplot as plt
from databases import query_station
import pandas as pd
from sklearn.metrics import auc 

class AutoCharge:
    """
    Class to define charging features for a single charging event
    """
    def __init__(self, data):
        
       # self.data = data
        data = data[1:] # remove the first point which is alwasy wrong
        self.voltage = data['hvBatteryVoltage']
        self.current = data['hvBatteryCurrent']
        self.power = abs(self.voltage * self.current)/1000 # Units: kW
        self.battery_temp = data['hvBatteryTemp']
        self.soc_display = data['hvSocActualDisplay']
        self.soc_bms = data['hvSocActualBms']
        self.latitude = data['latitude'].mean()
        self.longitude = data['longitude'].mean()
        self.timestamp = data['timestamp']
        self.car_model= data['model'].unique()[0]
        self.charging_time = self.getDelta('timestamp').total_seconds()/3600 #Units: hours
        self.charge_speed_soc = self.getDelta('soc_display')/self.charging_time
        self.charge_speed_power = self.getDelta('power')/self.charging_time
        self.voltage_net = self.getDelta('voltage') #net voltage charged
        self.current_net = abs(self.getDelta('current')) #net current flowed in the battery during charge
        self.power_net = self.getDelta('power') #net power charged

        cs = query_station(self.latitude, self.longitude)
        if len(cs) != 0:
            
            max_charge_power = self.power.max()
            cp = cs[cs.kw > max_charge_power]
            
            #try to guess which is the charge point id 
            if len(cp) == 1:
                            
                self.chargestation_id = cp.chargestation_id.values[0]
                self.chargepoint_id = cp.chargepoint_id.values[0]
                self.chargepoint_kw = cp.kw.values[0]
                self.chargepoint_type = cp.type.values[0]

            elif len(cp) > 1:
                            
                self.chargestation_id = cp.chargestation_id.unique()[0]
                self.chargepoint_id = cp.chargepoint_id.unique()[0]
                self.chargepoint_kw = cp[cp.chargepoint_id == self.chargepoint_id].kw.values[0]
                self.chargepoint_type = cp.type.values[0]
        
            else:
                
                self.chargestation_id = np.nan
                self.chargepoint_id = np.nan
                self.chargepoint_kw = np.nan
                self.chargepoint_type = np.nan
        else:
            
            self.chargestation_id = np.nan
            self.chargepoint_id = np.nan
            self.chargepoint_kw = np.nan
            self.chargepoint_type = np.nan
            
    def plot(self, X, Y):
        """
        Function to plot two attributes agains each other
        """
        plt.plot(getattr(self,X), getattr(self,Y), marker = 'o', alpha = 0.5)
        plt.ylabel(Y)
        plt.xlabel(X)
        plt.show()

    def timeplot(self, Y):
        """
        Function to plot attributes agains time
        """
        plt.plot(self.timestamp, getattr(self,Y), marker = 'o', alpha = 0.5)
        plt.ylabel(Y)
        plt.tick_params(axis='x',labelrotation=30)
        plt.xlabel('Timestamp')
        plt.show()

    def getDelta(self, att):
        """
        gets the net attribute difference during the charging event 
        """
        delta = getattr(self,att).max() - getattr(self,att).min() 
        return delta
    
class ChargingCurve(AutoCharge):
    """
    Class to analyse complete charging curve. For instance to determine complete 
    charge curve, extract ChargingPoint and temperature dependent Charge curves.
    """
    def __init__(self, data):
        
#        self.data = data[['soc_display','power', 'battery_temp', 'chargepoint_id', 'chargepoint_kw']]
        
        #sort data to build a ChargeCurve object
        p = pd.concat([charge.power for charge in data])
        soc = pd.concat([charge.soc_display for charge in data])
        t = pd.concat([charge.battery_temp for charge in data])
        cp = np.concatenate([[charge.chargepoint_id for n in range(len(charge.power))] for charge in data])
        kw = np.concatenate([[charge.chargepoint_kw for n in range(len(charge.power))] for charge in data])
        cptype = np.concatenate([[charge.chargepoint_type for n in range(len(charge.power))] for charge in data])
        
        # instantiate the ChargingCurve object
        curve = pd.DataFrame({'soc_display':soc, 'power':p, 'battery_temp':t, 'chargepoint_id':cp, 'chargepoint_kw': kw, 'chargepoint_type': cptype})
        curve = curve.sort_values('soc_display')
        curve = curve.reset_index(drop=True) # This solves a ValueError: cannot reindex from a duplicate axis
        self.data = curve
    
        ###############################
        ## generate statistics table ##
        ###############################
        cps = [p.chargepoint_id for p in data]
        cp_kw = [p.chargepoint_kw for p in data]
        model= [p.car_model for p in data]
        times= [p.charging_time for p in data]
        currents = [p.current_net for p in data] 
        voltages = [p.voltage_net for p in data] 
        power = [p.power_net for p in data] 
        soc_speed = [p.charge_speed_soc for p in data] 
        kw_speed = [p.charge_speed_power for p in data]
        temp_delta = [p.getDelta('battery_temp') for p in data] #net temp change during charge
        temps = [p.battery_temp.values.mean() for p in data] # mean temp during charge
        date = [p.timestamp.iloc[-1] for p in data] #get date from last timestamp record
        
        report = pd.DataFrame({'chargePoint':cps, 
                               'chargePoint_kw':cp_kw,
                               'car_model':model,
                               'current':currents,
                               'voltage':voltages,
                               'power':power,
                               'Charge_time':times,
                               'soc_speed':soc_speed,
                               'kw_speed':kw_speed,
                               'temps':temps,
                               'temp_delta':temp_delta,
                               'date':date
                               })
        
        self.report = report
        self.stats = report.groupby(['chargePoint', 'car_model']).agg(
                Nr_Charges = ('chargePoint','count'), 
                avg_Current = ('current', 'mean'),
                avg_Voltage = ('voltage', 'mean'),
                avg_Power = ('power', 'mean'),
                avg_ChargeTime = ('Charge_time', 'mean'),
                avg_ChargeSpeedSoc = ('soc_speed', 'mean'),
                avg_ChargeSpeedKw = ('kw_speed', 'mean'),
                avg_BatteryTemp = ('temps', 'mean'),
                avg_BatteryTempDelta = ('temp_delta', 'mean'),
        #        Nr_autos = ('car_model','count'),
                ).sort_values(by='Nr_Charges', ascending=False)
        
    
    
    def tempCurve(self, temp):
        """
        Extract Charging curve for a given temperature
        Returns a pandas DataFrame with data at the requested temperature
        """
        tc = self.data[self.data['battery_temp'] == temp]
        
        return tc
    
    def ChargePointCurve(self, chargePoint, temp=False, battCapacity=False):
        """
        Extracts Charging curve for a given ChargePoint.
        
        Parameters:
        -----------
        chargePoint : ID of the charge point to get charging curve
        temp : Temperature in °C, default is False
        battCapacity : Battery capacity in kWh, default is False
        
        Returns:
        ----------
        Returns a pandas DataFrame with data at the requested ChargePoint.
        If temp value is given, the returned data is that of the ChargingPoint
        at the requested temperature.
        If battCapacity is given, the resulting dataframe has information on charging time.
        """
        
        if temp:
            
            cp = self.data[(self.data['chargepoint_id'] == chargePoint) &\
                          (self.data['battery_temp'] == temp)]
        else:

            cp = self.data[self.data['chargepoint_id'] == chargePoint]
        
        # if the battery capacity is given, the time required to charge the battery is given
        if battCapacity:
            cp['remainingTime'] = battCapacity*(1-cp['soc_display'][1:]/100)/cp['power'][1:]
            cp['elapsedTime'] = abs((cp['remainingTime']*60).diff(1)).cumsum()
            
        return cp
    
    def ChargePowerCurve(self, power, temp=False, battCapacity=False):
        """
        Extracts Charging curve for a specified charger Power.
        
        Parameters:
        -----------
        power : Charger Power in kW
        temp : Temperature in °C, default is False
        battCapacity : Battery capacity in kWh, default is False
        
        Returns:
        ----------
        Returns a pandas DataFrame with data at the requested Charger power.
        If temp value is given, the returned data is at the requested temperature.
        If battCapacity is given, the resulting dataframe has information on charging time.
         """
        
        if temp:
            
            cp = self.data[(self.data['chargepoint_kw'] == power) &\
                          (self.data['battery_temp'] == temp)]
        else:

            cp = self.data[self.data['chargepoint_kw'] == power]
        
        # if the battery capacity is given, the time required to charge the battery is given
        if battCapacity:
            cp['remainingTime'] = battCapacity*(1-cp['soc_display'][1:]/100)/cp['power'][1:]
            cp['elapsedTime'] = abs((cp['remainingTime']*60).diff(1)).cumsum()
            
        return cp
    
#    def getPieceweise(self,curve, segments=5):
    
    def ChargePointCurveModel(self, chargePoint, temp = False):
        """
        Returns and polynomial fit model of the Charging Curve at the requested
        ChargePoint and Temperature if this is especified.
        
        """
        
        cp = self.ChargePointCurve(chargePoint, temp)
        x = cp.soc_display
        y = cp.power
    
        pol = np.polyfit(x, y,15)
        model = np.poly1d(pol)
        
        return model
    
    def plotCurve(self):
        """
        Plots the Charging Curve with all available data.
        """
        plt.plot(self.data.soc_display, self.data.power, 'o', alpha = 0.3)
        plt.xlabel('SoC (%)')
        plt.ylabel('Charging Power (kW)') 
        
    def getChargePointQuality(self,method = 'median'):
        """
        Generates a table with values like mean, median and area under curve (integration) 
        of the charging curve of the Charge Point and finds the Quality socre based on the method defined.
        
        Possible methods are: 'median', 'mean' and 'integral'
        
        The Q score is defined as value_i/max(value) for a given nominal Charger Power
        """

        methods = {'mean':'meanPower','median':'medianPower', 'integral':'IntegralPower'}
        
        cp_power = [p for p in self.data.chargepoint_kw.unique()]
        res_df = pd.DataFrame()
        for p in cp_power:
            mask = (self.data.soc_display > 20) \
            & (self.data.soc_display < 80) \
            & (self.data.chargepoint_kw == p)
            
            
            chargepoints = self.data[mask].chargepoint_id.unique()
        
            #extract some  metrics
            df = pd.DataFrame({'chargePoint': [cp for cp in chargepoints],
                             'meanPower': [self.ChargePointCurve(cp).power[mask].mean()
                                      for cp in chargepoints],
                             'medianPower': [self.ChargePointCurve(cp).power[mask].median()
                                      for cp in chargepoints],
                             'IntegralPower': [auc(self.ChargePointCurve(cp).soc_display[mask].values,
                                                   self.ChargePointCurve(cp).power[mask].values)
                                               if pd.notna(cp) else np.nan for cp in chargepoints]}
                            )
            
            df_sorted = df.sort_values(by=methods[method], ascending=False)
            df_sorted['chargePoint_quality'] = df_sorted[methods[method]]/df_sorted[methods[method]].max()
            df_sorted['chargePoint_quality_int'] = df_sorted[methods['integral']]/df_sorted[methods['integral']].max()
            df_sorted['chargePoint_power'] = p
            res_df = res_df.append(df_sorted)
            
        return res_df.dropna()

class AutoDrive:
    """
    Class to define drive features for a single driving event
    """
    def __init__(self, data):
        for attr in data.columns.values:
            setattr(self, attr, data[attr])

        self.model = data['model'].unique()[0]
        self.distance = self.getDelta('odometer') # units km
#        self.power = abs(self.hvBatteryVoltage * self.hvBatteryCurrent / 1000) # units kW

        self.power = self.hvBatteryVoltage * self.hvBatteryCurrent / 1000
        # positive power = consumption
        # negative power = gain
        self.power_consumption = self.power.apply(lambda x: 0 if (x < 0) else x)# units kW
        self.power_gain = self.power.apply(lambda x: 0 if (x > 0) else abs(x))# units kW
        self.drive_time = self.getDelta('timestamp').total_seconds()/3600 # Units: hours
        self.speed_ave = self.speed.mean()
        self.power_consumption_ave = self.power_consumption.mean()
        self.power_gain_ave = self.power_gain.mean()
        self.power_consumption_net = self.power_consumption_ave - self.power_gain_ave
        self.soc_net = self.getDelta('hvSocActualDisplay')

    def plot(self, X, Y):
        """
        Function to plot two attributes agains each other
        """
        plt.plot(getattr(self,X), getattr(self,Y), marker = 'o', alpha = 0.5)
        plt.ylabel(Y)
        plt.xlabel(X)
        plt.show()

    def timeplot(self, Y):
        """
        Function to plot attributes agains time
        """
        plt.plot(self.timestamp, getattr(self,Y), marker = 'o', alpha = 0.5)
        plt.ylabel(Y)
        plt.tick_params(axis='x',labelrotation=30)
        plt.xlabel('Timestamp')
        plt.show()

    def getDelta(self, att):
        """
        gets the net attribute difference during the charging event 
        """
        delta = getattr(self,att).max() - getattr(self,att).min() 
        return delta

class DriveCurve(AutoDrive):
    """
    add docstring
    """
    def __init__(self, data):
        
        #check integrity
        for v in data:
            if v.distance < 5 or v.distance > 400:
                data.remove(v)
        
        
        #sort data to build a ChargeCurve object
        pcn = pd.concat([drive.power for drive in data])
        pc = pd.concat([drive.power_consumption for drive in data])
        pg = pd.concat([drive.power_gain for drive in data])
        soc = pd.concat([drive.hvSocActualDisplay for drive in data])
        t = pd.concat([drive.hvBatteryTemp for drive in data])
        s = pd.concat([drive.speed for drive in data])
        a = pd.concat([drive.altitude for drive in data])

        # instantiate the ChargingCurve object
        curve = pd.DataFrame({'SocDisplay':soc, 
                              'PowerConsumptionNet':pcn, 
                              'PowerConsumption':pc, 
                              'PowerGain':pg, 
                              'BatteryTemp':t,
                              'Speed':s,
                              'Altitude':a,
                              })
    
        curve = curve.sort_values('Speed')
        curve = curve.reset_index(drop=True) # This solves a ValueError: cannot reindex from a duplicate axis
        self.data = curve
    
        ###############################
        ## generate statistics table ##
        ###############################
        model= [p.model for p in data]
        distances = [p.distance for p in data]
        times = [p.drive_time for p in data]
        speeds = [p.speed_ave for p in data]
        cons = [p.power_consumption_ave for p in data] 
        gain = [p.power_gain_ave for p in data] 
        cons_net = [p.power_consumption_net for p in data] 
        soc_net = [p.soc_net for p in data] 
        temp_delta = [p.getDelta('hvBatteryTemp') for p in data] #net temp change during charge
        temps = [p.hvBatteryTemp.values.mean() for p in data] # mean temp during charge
        date = [p.timestamp.iloc[-1] for p in data] #get date from last timestamp record
        
        report = pd.DataFrame({'CarModel':model,
                               'DriveDistance':distances,
                               'DriveTime':times,
                               'Speed_mean':speeds,
                               'PowerConsumptionNet':cons_net,
                               'PowerConsumption':cons,
                               'PowerGain':gain,
                               'SocNet':soc_net,
                               'BatteryTemp_mean':temps,
                               'BatteryTemp_delta':temp_delta,
                               'date':date
                               })
        
        self.report = report
        self.stats = self.report.groupby(['CarModel']).agg(
                Nr_Drives = ('CarModel','count'), 
                avg_PowerConsumptionTotal = ('PowerConsumption', 'mean'),
                avg_PowerGainTotal = ('PowerGain', 'mean'),
                avg_PowerConsumptionNet = ('PowerConsumptionNet', 'mean'),
                avg_DriveDistance = ('DriveDistance', 'mean'),
                avg_SoC= ('SocNet', 'mean'),
                avg_DriveTime = ('DriveTime', 'mean'),
                avg_Speed = ('Speed_mean', 'mean'),
                avg_BatteryTemp = ('BatteryTemp_mean', 'mean'),
                avg_BatteryTempDelta = ('BatteryTemp_delta', 'mean'),
                ).sort_values(by='Nr_Drives', ascending=False)
        
    def plot_EfficiencyCurve(self, method = 'median', temp=False):
        
        if temp:
            mask = (self.data.BatteryTemp == temp)
            x = self.data.Speed[mask]        
            y = self.data.PowerConsumptionNet[mask]
            
        else:
            x = self.data.Speed        
            y = self.data.PowerConsumptionNet

        y_mod = self.EfficiencyCurveModel(method=method, temp=temp)(x)
        
        
        battery = 38.2 #kWh

        fig, (ax1, ax2, ax3) = plt.subplots(3,1 ,figsize=(10,10))
        #plot consumed power vs. speed
        ax1.plot(x, y, '.', alpha = 0.2, label= 'data')
        
        if method == 'rolling':
            ax1.plot(x, y.rolling(50).median(), '.',alpha=0.3,  c='y', label= 'Moving median')
        else:
            medians = self.getMedianBins(temp=temp)
            ax1.plot(medians.Speed, medians.PowerConsumptionNet, 'o', c='g', label= 'Median')

        ax1.plot(x, y_mod, c='red', lw=2,label= 'model')
        ax1.set_ylabel('Consumed Power [kW]')
        ax1.set_ylim(-10,60)
        ax1.legend()
        
        #plot Efficiency vs. speed
        ax2.plot(x, 100*y_mod/x, c='red', lw=2,label= 'model')
        ax2.set_ylabel('Efficiency [kWh/100km]')
        ax2.set_ylim(0,40)
        
        #plot range vs. speed
        ax3.plot(x, battery/(y_mod/x), c='red', lw=2,label= 'model')
        ax3.set_ylabel('Range [km]')
        ax3.set_ylim(0,(battery/(y_mod/x)).max()+100)
        plt.xlabel('Speed [km/h]')
        plt.tight_layout()
        
    def EfficiencyCurveModel(self, method ='median', temp = False):
        """
        Returns and polynomial fit model of the Battery consupmtion vs Speed
        
        Parameters:
            method = used method to estimate the Efficiency model. Options are
                    'median' and 'rolling' 
        """
        if method == 'median':
            medians = self.getMedianBins(temp=temp)
            #mask to remove NaN values (needed to perform the fit)
            mask = np.isfinite(medians.Speed) 
#            mask = np.isfinite(medians.Speed) & medians.PowerConsumption !=0

            x = medians.Speed[mask]
            y = medians.PowerConsumptionNet[mask]

            #fit model using the median values at constants speed
            pol = np.polyfit(x, y, deg=2, full=True)
            model = np.poly1d(pol[0])

        elif method == 'rolling':
            ma = 50 #rolling period
            if temp:
                mask = (self.data.BatteryTemp == temp)
                x = self.data.Speed[mask]
                y = self.data.PowerConsumptionNet[mask] 
                
            else:
                x = self.data.Speed
                y = self.data.PowerConsumptionNet 
            
            y1 = y.rolling(ma).median()

            #fit model using moving median with a ma period window
            pol = np.polyfit(x.iloc[ma:], y1[ma:], deg=2, full=True)
            model = np.poly1d(pol[0])
        else:
            print("Available methods are 'median' and 'rolling'")

        return model
    
    def getMedianBins(self, temp = False):
        """
        Function to determine the median value at a constant speed, by aggregating
        the power values in 1 km/h speed bins.
        
        Returns a panda DataFrame with median values of Speed, PowerConsumption, 
        PowerNet at 1 km/h bins.
        """
        
        bins = pd.IntervalIndex.from_tuples([(i, i+1)  for i in range(152)])
        
        if temp:
            mask = (self.data.BatteryTemp == temp)
            dff = self.data[['Speed', 'PowerConsumptionNet', 'PowerConsumption', 'BatteryTemp']][mask]
        else:
            dff = self.data[['Speed', 'PowerConsumptionNet', 'PowerConsumption', 'BatteryTemp']]
            
        dff['ranges'] = pd.cut(dff.Speed, bins = bins)
    
        medians = dff.groupby('ranges').median()
        
        return medians