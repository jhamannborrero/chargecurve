#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 30 14:25:19 2020

@author: jehamann
"""
from scipy.signal import find_peaks
import numpy as np
from chargecurve import AutoCharge, AutoDrive
import datetime
import scipy.constants as ct
import pandas as pd


##############
## Funtions ##
##############
        
#collect all data for each charging curve at store it as AutoCharge() objects
def collect_charges(df, peaks_id):
    """
    Usage: collect_charges(df, peaks_id)
    
    df: Pandas DataFrame with the battery charging data.
    peak_id: List with the index positions used to determine start and 
    end of charging event
 
    Returns a list where each element is an AutoCharge() object.        
    """
    
    #cols to collect
    cols =['hvBatteryCurrent','hvBatteryVoltage','hvBatteryTemp','hvSocActualDisplay', 
           'hvSocActualBms','timestamp', 'latitude', 'longitude','model']

    #check that the peaks_id list contains even number of elements
    if len(peaks_id) % 2 == 0:
            
        charges = [AutoCharge(df.iloc[peaks_id[i]:peaks_id[i+1]][cols].\
                                  drop_duplicates(subset='hvSocActualDisplay', keep='last')) \
                       for i in np.arange(0,len(peaks_id),2)]
    else:

        charges = [AutoCharge(df.iloc[peaks_id[i]:peaks_id[i+1]][cols].\
                                  drop_duplicates(subset='hvSocActualDisplay', keep='last')) \
                       for i in np.arange(0,len(peaks_id[1:]),2)]
        
        
    #remove charges that take longer than 5 Hours, i.e., wrongly estimated
    for i, v in enumerate(charges):
        if v.charging_time > 5:
            charges.pop(i)
    
    return charges

#collect all data for each drive and stores it as AutoDrive() objects
def collect_drives(df):
    """
    Usage: collect_drives(df, peaks_id)
    
    df: Pandas DataFrame with the drive data.
    peak_id: List with the index positions used to determine start and 
    end of drive event
 
    Returns a list where each element is an AutoDrive() object.        
    """
    # add timedeltas
    dff = df.copy() 
    dff['timedelta'] = dff.timestamp.diff(1)
    # if time delta is larger than 10 mins, this will be considered aonther ride
    peaks_idx = dff[dff.timedelta > datetime.timedelta(seconds=600)] 
    # add initial index value and sort index
    peaks_idx = peaks_idx.append([dff.iloc[0]]).sort_index()
    peaks_idx = peaks_idx.index.values

    drives = [AutoDrive(dff.loc[peaks_idx[i]:peaks_idx[i+1]][:-1]) \
              for i in range(len(peaks_idx[:-1]))]

    #remove drives that are too short (less than 4km) or too large (wrongly determined)
#    for d in drives:
#        if d.distance < 5 or d.distance > 400:
#            drives.remove(d)

    return drives

#def get_peaks(df, col):
#    """
#    Function to get index values where there are peaks in the data (maxima and minima)
#    df: dataframe containing the data to a analize
#    col (string): col name used to identify peaks 
#    """
#    # get maxima
#    max_id =find_peaks(df[col].values)[0]
#    #get minima
#    min_id =find_peaks(-1*df[col].values)[0]
#    
#    peaks_id = np.append(min_id, max_id)
#    #add initial and final points
#    peaks_id = np.append(peaks_id,[0,len(df)-1])
#    peaks_id.sort()
#    
#    return peaks_id

def get_peaks(df, col, kind='charge'):
    """
    Function to get index values where there are peaks in the data (maxima and minima)
    df: dataframe containing the data to a analize
    col (string): col name used to identify peaks 
    """
    
    if kind == 'charge':
        # get maxima
        max_id =find_peaks(df[col].values)[0]
        #get minima
        min_id =find_peaks(-1*df[col].values)[0]

    elif kind == 'drive':
        # get maxima
        max_id =find_peaks(df[col].values, prominence=2)[0]
        #get minima
        min_id =find_peaks(-1*df[col].values, prominence=2)[0]
        
    peaks_id = np.append(min_id, max_id)
    #add initial and final points
    peaks_id = np.append(peaks_id,[0,len(df)-1])
    peaks_id.sort()
    
    return peaks_id

def P_heating(T_in, T_out, T_target):
    """
    Calculates the power required to heat the air cabin of an auto
    
    Parameter:
    ----------
    T_in: Temperature inside the car (°C)
    T_out: Temperature outside the car (°C)
    T_target: Target temperature inside the car (°C)
    
    Returns:
    --------
    Power in kW    
    """
    t_in = 273.15 + T_in
    t_out = 273.15 + T_out
    t_target = 273.15 + T_target

    p = 0.2*abs(t_in-t_target) + 0.02*abs(t_in-t_out)
    
    return p

def E_aero(speed, temp, area, c_drag, altitude, distance, wind=0):
    """
    Calculates the aerodynamic Energy for the vehicle.
    
    Parameters:
    -----------
    speed : car speed/velocity in km/h 
    temp : Temperature in °C
    area : Frontal area of the vehicle in m^2
    c_drag : drag coefficient of the vehicle
    altitude : altitude in m
    wind : wind velocity in km/h
    distance : Total drive distance (m)
    
    Returns:
    --------
    This function returns a 'float' with the value of the erodynamic Energy in kWh
    """
    
    p_0 = 101325# Sea level standard atmospheric pressure Pa (N/m^2)
    M = 0.02896968 # Molar mass of dry air kg/mol
    
    #unit conversion
    T = temp + 273.15 # °C to K
    v = speed / 3.6 # km/h to m/s
    wind = wind / 3.6
    
    #barometric formula
    P_air = p_0*np.exp((-1 * M * ct.g * altitude)/(ct.R * T))
    
    # air density as function of Temp and Pressure
    rho_air = P_air * M / (ct.R * T)
    
    E = (0.5 * rho_air * area *c_drag * (v - wind)**2 * distance)/3.6e6
    
    return E


def E_fric(mass, distance, h_start, h_end, wet=False):
    """
    Calculates the frictional Energy for the vehicle.
    
    Parameters:
    -----------
    mass : mass of the car in kg 
    distance : Total drive distance (m)
    h_start : altitude at initial location (m)
    h_end :  altitude at final location (m)
    wet : boolean, tell if the road is wet. Default is False
    
    Returns:
    --------
    This function returns a 'float' with the value of the frictional Energy in kWh
    """
    if wet:
        mu = 0.013 # friction coefficient between wet Tires and wet asphalt
    else:
        mu = 0.011 # friction coefficient between dry Tires and dry asphalt
        
    E = (mu * mass * ct.g * np.sqrt(distance**2 - (h_end - h_start)**2))/3.6e6
    
    return E

def E_grav(h_start, h_end, mass):
    """
    Returns the energy in kWh required to bring the car from a height h_start
    to a height h_end 
    
    Parameters:
    -----------
    mass : mass of the car in kg 
    h_start : altitude at initial location (m)
    h_end :  altitude at final location (m)
    
    Returns:
    --------
    This function returns a 'float' with the value of the grav Energy in kWh
    """
    
    e = mass * ct.g * (h_end - h_start)/3.6e6 
    return e

def autoEnergy(auto, add_weight, distance, t_in, t_out, h_start, h_end, soc_start, chargePointkW, wind, wet):
    """
    """
    #
    Eff = 0.9 # motor efficiency
    
    velocities = [vel for vel in range(10,210,10)]
    
    mass = auto.emptyWeight[0] + add_weight
    
    Energy = pd.DataFrame({'Velocity [km/h]':[vel for vel in velocities],
                           'E aero [kWh/100km]':[E_aero(vel,t_out,auto.area[0], auto.dragFactor[0], h_start,
                                                     distance, wind) * 100/(distance/1000)
                                                 for vel in velocities],
                           'E fric [kWh/100km]':[E_fric(mass, distance, h_start, h_end, wet) * 100/(distance/1000)
                                                 for vel in velocities],
                           'E gravity [kWh/100km]':[E_grav(h_start, h_end, mass) * 100/(distance/1000)
                                                    for vel in velocities],
                           # here it is assumed that the car cold/warm at the beginning at the target T
                           'E heating [kWh/100km]': [P_heating(t_out, t_out, t_in) * 100/vel 
                                                     for vel in velocities], 
                           'E electric [kWh/100km]': [0.5 * 100/vel 
                                                      for vel in velocities],
                           })
    

    # still need to add efficiency of the motor/battery (wirkungsgrad)
    Energy['E Total [kWh/100km]'] = (Energy['E aero [kWh/100km]'] 
                                           + Energy['E fric [kWh/100km]'] 
                                           + Energy['E gravity [kWh/100km]'] 
                                           + Energy['E heating [kWh/100km]'] 
                                           + Energy['E electric [kWh/100km]']
                                          )/Eff
    
    Energy['Range [km]'] = auto.nettoKwh[0]*100/Energy['E Total [kWh/100km]']
    
    Energy['E demand trip [kWh]'] = Energy['E Total [kWh/100km]']*(distance/1000)/100
    
    Energy['E charge [kWh]'] = Energy['E demand trip [kWh]'].apply(
        lambda x: x - (auto.nettoKwh[0] * soc_start/100) + auto.nettoKwh[0]/4 # add 1/4 of battery for safety
        if (auto.nettoKwh[0] * soc_start/100) < x 
        else 0)
    
    Energy['Charging Stops'] =  np.ceil(Energy['E charge [kWh]']/auto.nettoKwh[0])
    
    # need to add efficiency of the chargePoint (more energy/time needed as planned)
    Energy['Charging time [h]'] =(Energy['E charge [kWh]']/chargePointkW) + Energy['Charging Stops']*(5/60)*2 # add 5 min/charge stop

    Energy['Driving time [h]'] = (distance/1000)/Energy['Velocity [km/h]']

    Energy['Total time [h]'] =  Energy['Driving time [h]'] + Energy['Charging time [h]']

    return Energy

def getArcEnergy(auto, add_weight, distance, t_in, t_out, t_target, h_start, h_end, speed, wind=0, wet=False):
    
    Eff = 0.9 # motor efficiency
    
    mass = auto.emptyWeight[0] + add_weight
    c_drag = auto.dragFactor[0]
    area = auto.area[0]
    
    E_f = E_fric(mass, distance, h_start, h_end, wet=wet)
    E_a = (E_aero(speed,t_out,area, c_drag, h_start,distance, wind=wind) + E_aero(speed,t_out,area, c_drag, h_end,distance, wind=wind))/2
    E_g = E_grav(h_start, h_end, mass)
    E_h = P_heating(t_in, t_out, t_target) * (distance/1000)/speed
    E_e = 0.5 * (distance/1000)/speed
    
    Energy = (E_f + E_a + E_g + E_h + E_e)/Eff
    return Energy

