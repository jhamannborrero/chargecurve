#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 30 14:19:15 2020

@author: jehamann
"""
import matplotlib.pyplot as plt
import numpy as np
###################
## Plot Funtions ##
###################
        
def charges(df, peaks_id):
    """
    Plots the charging events on the car together with the peak used to determine
    the starting and ending of the charge.
    
    Usage: charges(df, peaks_id)
    df: Pandas DataFrame with the charging data
    peak_id: List with the index positions used to determine start and 
    end of charging event
    """
    plt.plot(df.timestamp.iloc[peaks_id], df.hvSocActualDisplay.iloc[peaks_id], 'o', c='red', alpha=0.5 )
    plt.plot(df.timestamp, df.hvSocActualDisplay, '.:')
    plt.tick_params(axis='x',labelrotation=30)
    plt.ylabel('SoC (%)')
    plt.tight_layout()

def chargecurvestation(data):
    """
    Plots charging curves for each charge station.
    
    Usage: chargecurvestation(data)
    data: list object where each element is a AutoCharge() object.
    """
#    stations = set([car.chargestation_id for car in data])
    stations = set([car.chargepoint_id for car in data])

    for station in stations:
        if station is np.nan:
            pass
        else:
            fig, ax = plt.subplots(figsize=(7,4))

            for car in data:
#                if car.chargestation_id == station:
                if car.chargepoint_id == station:
                    ax.plot(car.soc_display, car.power, 'bo', alpha = 0.3)
#                    plt.title(f'Charge Station Id: {station}')
                    plt.title(f'Charge Point Id: {station}, {car.chargepoint_kw}kW')
                    plt.ylabel('Charging Power (kW)')
                    plt.xlabel('SoC Display (%)')

def fullchargecurve(charges, temp=False):
    """
    Plots full charging curves for a battery.
    
    Usage: fullchargecurve(charges, temp=False)
    charges: list object where each element is a AutoCharge() object.
    temp: if True the plot displays the points with colors representing the battery
    temp.S
    """

    #fig, ax = plt.subplots()#figsize=(7,4))
    
    p = []
    soc = []
    t = []
    
    for charge in charges:
        p = np.append(p, charge.power.values)
        soc = np.append(soc, charge.soc_display.values)
        t = np.append(t, charge.battery_temp.values)        

    if temp:
        
        plt.scatter(soc,p,c=t, marker='o', alpha = 0.5, cmap='jet')
        cbar= plt.colorbar()
        cbar.ax.set_ylabel('Temperature(°C)')
#        plt.legend(*scatter.legend_elements(), title='Temp', loc='right')
    else:

        plt.plot(soc, p, 'bo', alpha = 0.3)

    #plt.title(f'Charge Station Id: {station}')
    plt.ylabel('Charging Power (kW)')
    plt.xlabel('SoC Display (%)')