#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb  6 16:54:54 2020

@author: jehamann
"""

from chargecurve import ChargingCurve
import pandas as pd
from databases import load_base2_ftp
from functions import get_peaks, collect_charges
import datetime

##load data
hyundai = load_base2_ftp('ecu_bcddc2d07024')
hyundai = hyundai[hyundai.model == 'HYUNDAIIONIQ']
#
##select while charging data, for Hyundai the conditions are as below
mask = (hyundai.hvBatteryCurrent < 0) & (hyundai.rpmEmotor <=0) \
& (hyundai.timestamp > datetime.datetime(2019,12,1)) 
#
##find peaks to extract charging curves
peaks_id = get_peaks(hyundai[mask], 'hvSocActualDisplay')
#
hyundai_charges = collect_charges(hyundai[mask], peaks_id)
#
## instantiate the ChargingCurve object
HyundaiChargeCurve = ChargingCurve(hyundai_charges)
#
##load kia data
kia = load_base2_ftp('ecu_bcddc2d0718c')
kia = kia[kia.model == 'KIAENIRO']
#
##select while charging data, for kia the conditions are as below
mask = (kia.hvBatteryCurrent < 0) & (kia.rpmEmotor <=0) \
#
##find peaks to extract charging curves
peaks_id = get_peaks(kia[mask], 'hvSocActualDisplay')
#
kia_charges = collect_charges(kia[mask], peaks_id)
#
## instantiate the ChargingCurve object
KiaChargeCurve = ChargingCurve(kia_charges)
#
##load tesla data
tesla = load_base2_ftp('ecu_bcddc2ceb734')
tesla = tesla[tesla.model == 'TESLA_S']
#
##select while charging data, for Tesla the conditions are as below
mask = (tesla.hvBatteryCurrent > 0) & (tesla.rpmEmotor <=0) 
#
##find peaks to extract charging curves
peaks_id = get_peaks(tesla[mask], 'hvSocActualDisplay')
#
##collect all data for each charging curve at store it as AutoCharge() objects
tesla_charges = collect_charges(tesla[mask],peaks_id)
#
## instantiate the ChargingCurve object
TeslaChargeCurve = ChargingCurve(tesla_charges)
#
#
AllChargeCurves = ChargingCurve(tesla_charges + kia_charges+ hyundai_charges)


scores = pd.DataFrame()
curve_dict = {0:'TESLA_S',1:'KIAENIRO', 2:'HYUNDAIIONIQ'}
for i, curve in enumerate([TeslaChargeCurve,KiaChargeCurve, HyundaiChargeCurve]):
    score = curve.getChargePointQuality()
    score['car_model'] = [curve_dict[i] for n in range(len(score))]
#    score = score['chargePoint_quality']
    scores = pd.concat([scores, score])
scores  = scores.set_index(['chargePoint', 'car_model'])[['chargePoint_quality', 'chargePoint_power']]

allcharges_stats = AllChargeCurves.stats
report = allcharges_stats.join(scores,how='left')
print(report)
