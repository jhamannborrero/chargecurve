#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 15 14:59:00 2020

@author: jehamann
"""
import pandas as pd
import sys, warnings
sys.path.append('../chargecurve/')
warnings.filterwarnings('ignore')
from databases import query_vehicle
from functions import getArcEnergy
import openrouteservice as ors
from openrouteservice import client
import folium 
import geopy.distance 
import psycopg2
import numpy as np

def getGeoDistance(line):
    """
    Calculates the distance between two points based on its latitude and longitude
    
    Parameter:
    ----------
    line : Route Profile (pandas DataFrame) with lat and long info
    
    Returns:
    --------
    two lists, one with the distance between each point and the other with the 
    cumulative sum of the distances.
    """
    
    x0 = list(zip(line['lat'], line['long']))
    x1 = x0[1:]
    s = [0]
    for i in range(len(x1)):
        s.append(geopy.distance.distance(x0[i],x1[i]).km)
    
    return s, np.cumsum(s)


def queryRouteStations(route, cp_type):
    """
    Query the Charge Columns along the supplied route from 'stations' DB.
    Each 5km charge columns are search within a 25km^2 area.
    
    Parameters:
    ----------
    route (DataFrame): pandas DataFrame with latitude, longitude of the route
    cp_type (string): Charger type e.g CCS, ChaDeMo, Typ2.
    
    Returns:
    --------
    Returns a pandas DataFrame with charge point information located along the route.
    """
    
    #PostgreSQL connection (Charge Station data)
    pg_host = "10.19.160.198"
    pg_port = "5432"
    pg_user = "stations-api"
    pg_password = "Zj9XkLMu8BCJ3_sK7WGYqhD"
    database = "stations"

    con = psycopg2.connect(database=database, user=pg_user, password=pg_password, host=pg_host, port=pg_port)

    delta = 0.02   # +/-delta range for the latitude/longitude
    
    # define distance between point locations
    route['distance']= np.round(getGeoDistance(route)[1])
    # filter location points that are 5km away from each other
    spaced = route[route.distance % 5 == 0].drop_duplicates(subset='distance', keep='first')
    lat = spaced.lat
    lon = spaced.long
    
    # define query at the stations DB for the location along the route
    def query(lat, lon, delta, cp_type):
        s = "SELECT cs.longitude, cs.latitude, cs.\"addressStreet\", cs.id AS chargestation_id, cp.id AS chargepoint_id, cp.kw, cp.ampere, cp.volt, cp.type "\
        "FROM \"chargepoints\" AS cp LEFT JOIN \"chargegroups\" AS cg ON cg.id = cp.chargegroup_id "\
        "LEFT JOIN \"chargestations\" AS cs ON cs.id = cg.chargestation_id "\
        "WHERE latitude BETWEEN {} AND {} AND longitude BETWEEN {} AND {} AND type = '{}';".format(lat-delta, lat+delta,lon-2*delta, lon+2*delta,cp_type)
        
        return s
    
    cs = pd.concat(pd.read_sql_query(query(lat.values[n], lon.values[n], delta, cp_type),con) 
                   for n in range(len(lat)))
    con.close()
    cs = cs.drop_duplicates(subset='chargestation_id')

    return cs

def getRouteData(origin, destination, speed=110, vehicle='IONIQ Elektro', nr_stations = 59):
    

    # get route from ors
    api_key = '5b3ce3597851110001cf62487d4fb586fbde48c4a1920afb15fe4220' #Provide your personal API key
    clnt = client.Client(key=api_key) 

    route = ors.directions.directions(clnt, coordinates=[origin, destination], profile = 'driving-car',
                                      instructions=False)

    geom = route['routes'][0]['geometry']

    # extracts lat, lon and altitude
    route_profile = pd.DataFrame(ors.convert.decode_polyline(geom,is3d=False)['coordinates'], 
                                 columns=['long','lat'])

    #query stations along the route
    stations_df = queryRouteStations(route_profile, cp_type='CCS')
    stations_df = stations_df[stations_df.kw.isin([50,100,150,350])] # get colums with the availabe charge curves

    # randomly extract nr_stations stations from the route due to CPLEX limitations
    stations_df = stations_df.sample(nr_stations, weights='kw')#, random_state=11)

    # sort stations distance from origin
    stations_df['dist_to_origin'] = [geopy.distance.distance(x, origin[::-1]).km*1000 
                                     for x in list(zip(stations_df.latitude, stations_df.longitude))]
    stations_df = stations_df.sort_values(by='dist_to_origin')


    stations_loc = list(zip(stations_df.longitude,stations_df.latitude ))

    stations = {i+1:{'power': stations_df.kw.iloc[i],
                                           'type':stations_df.type.iloc[i],
                                           'location':stations_loc[i]
                                          }
                for i in range(len(stations_df))}

    nodes = [0, *[s for s in stations]]
    nodes = [*nodes, len(nodes)]

    nodes_loc = [tuple(origin), *[stations[s]['location'] for s in stations], tuple(destination)]

    # get distance matrix from ors (limited to 59 locations)
    matrix = ors.distance_matrix.distance_matrix(clnt, locations=nodes_loc, profile = 'driving-car',
                                                 metrics=['distance'])
    matrix = matrix['distances']

    # get elevations from ors 
    elevation = ors.elevation.elevation_line(clnt, format_in='polyline', geometry=nodes_loc, 
                                             format_out='polyline')
    elevation = elevation['geometry']

    # get vehicle information for energy calculation
    auto = query_vehicle(vehicle)
    add_weight = 0
    t_in = 20
    t_out = 20

    # build data structures
    arcs = {(i,j):{} for i in nodes 
            for j in nodes if i!=j and i<j}

    for i in nodes:
        for j in nodes:
            if i!=j and i<j:
                arcs[i,j]['distance'] = matrix[i][j] 
                arcs[i,j]['time'] = round(arcs[i,j]['distance']*60/(1000*speed),1)
                arcs[i,j]['consumption'] = round(getArcEnergy(auto, add_weight, 
                                                              distance = matrix[i][j], 
                                                              t_in=t_in, 
                                                              t_out=t_out,
                                                              t_target=20,
                                                              h_start=elevation[i][2], 
                                                              h_end=elevation[j][2], 
                                                              speed=speed, 
                                                              wind=0, 
                                                              wet=False),1)

    # remove unfeasible arcs (optimizes the MILP solution)            
    for arc in [arc for arc in arcs if arcs[arc]['consumption']>auto.nettoKwh[0]]:
        del arcs[arc]

    arcs['nodes'] = {node:{'location': nodes_loc[node]} for node in nodes }

    return arcs, stations, auto

from docplex.mp.model import Model

def solveEVRP(arcs, stations, auto, soc_ini, soc_min, soc_end):
    
    overhead = 10 # mins
    arcs = arcs.copy()
    origin = arcs['nodes'][0]
    destination = arcs['nodes'][-1]
    nodes =  arcs['nodes']

    del arcs['nodes']
    
    Q = auto.nettoKwh[0]
    
    mdl=Model('EVRP')

    # decision variables flow graph

    # boolean variable used to activate an arc
    x=mdl.binary_var_dict(arcs,name='x')

    # defines battery state of charge at nodes
    c_in = mdl.continuous_var_dict(nodes[1:], lb=Q*soc_min, name = 'c_in') # stations + destination
    c_out = mdl.continuous_var_dict(nodes[:-1], lb=Q*soc_min, ub=Q, name = 'c_out') # origin + stations

    # defines charging times at stations
    tci = mdl.continuous_var_dict(stations, name = 'tci') # only stations
    tcf = mdl.continuous_var_dict(stations, name = 'tcf') # only stations

    # defines the quantity that flows through the arc (energy consummed during the drive from i to j) 
    q = mdl.continuous_var_dict(arcs, ub=Q*(1-soc_min), name = 'q')
    
    # objective function: minimize the travel time
    mdl.minimize(mdl.sum(arcs[arc]['time']*x[arc] for arc in arcs) 
                 + overhead*(mdl.sum(x[arc] for arc in arcs) - 1) 
                 + mdl.sum((tcf[j]-tci[j]) for j in stations) 
                )

    # constraints

    # if x[i,j] -> defines the value if q[i,j] (flow quantity) to be equal to the engy consumption of the arc 
    mdl.add_indicator_constraints(mdl.indicator_constraint(x[arc],q[arc]==arcs[arc]['consumption']) 
                                  for arc in arcs)

    #battery charge constraints: 
    mdl.add_indicator_constraints(mdl.indicator_constraint(x[i,j],c_out[i]-c_in[j]==q[i,j]) 
                                  for i in nodes for j in nodes if (i,j) in arcs )

    mdl.add_constraint(c_out[origin]==Q*soc_ini) # c_out from origin
    mdl.add_constraint(c_in[destination]==Q*soc_end) # c_in at destination

    # charger time
    curves = {power:mdl.piecewise(*pwf_cplex_params_dic[power]) for power in pwf_cplex_params_dic}
    
    # charge time as normal constrain
    mdl.add_constraints(tci[j] == curves[stations[j]['power']](c_in[j]*100/Q) for j in stations)

    mdl.add_constraints(tcf[j] == curves[stations[j]['power']](c_out[j]*100/Q)
                        for j in stations)

    mdl.add_constraints(tcf[j] >= tci[j] for j in stations)

    # forces flow from starting point
    mdl.add_constraint(mdl.sum(x[origin,j] for j in nodes if j !=origin and (origin,j) in arcs) == 1 )

    # flow constraint
    for i in nodes:
        mdl.add_constraint(mdl.sum(x[j,i] for j in nodes if i!=j and (j,i) in arcs) == 
                           mdl.sum(x[i,j] for j in nodes if i!=j and (i,j) in arcs),'flow{}'.format(i) )

    # deletes constraint that is no required and blocks the flow
    for i in nodes:
        if i not in stations: # not in stations
            mdl.remove_constraint('flow{}'.format(i))

    # print model
    mdl.parameters.timelimit=30
    #print(mdl.export_to_string())
    #print(mdl.print_information())
    
    # Solve model
    solucion=mdl.solve()#log_output=True)
    print(mdl.get_solve_status())
    #print(solucion.display())

    arcos_activos=[k for k in arcs if x[k].solution_value>0.9]

    stations_sol =[]
    #check times
    print('+'*100)
    print('Station\t\t\t\t\tPower\tBattery in\tBattery out \tCharge Time')
    print('\t\t\t\t\t[kW]\t[kWh]\t\t[kWh]\t\t[min]')
    print('='*100)
    print('{}\t\t\t{}\t{}\t\t{:.1f} ({:.1f}%)\t\t{}'.format(origin, '','',  c_out[origin].solution_value,c_out[origin].solution_value*100/Q, '') )
    for sol in tcf:
        tf =tcf[sol].solution_value
        ti = tci[sol].solution_value
        delta = tf-ti
        ci = c_in[sol].solution_value
        co = c_out[sol].solution_value
        ctype = stations[sol]['power']
        if delta !=0:
            stations_sol.append(sol)
            print('{}\t{}\t{:.1f} ({:.1f}%)\t{:.1f} ({:.1f}%)\t{:.1f}'.format(sol,ctype, ci, ci*100/Q, co, co*100/Q, delta) )
    print('{}\t\t\t\t{:.1f} ({:.1f}%)\t{}\t{}'.format(destination, c_in[destination].solution_value, c_in[destination].solution_value*100/Q, '', '') )
    print('+'*100)

    tot_dis = sum(arcs[arc]['distance'] for arc in arcos_activos )
    print('Total Distance traveled: {:.1f} km'.format(tot_dis/1000))
    
    TotTime = divmod(mdl.objective_value, 60)
    
    chargeTime = divmod(sum([tcf[sol].solution_value - tci[sol].solution_value for sol in tcf]), 60)
    driveTime = divmod(sum([arcs[arc]['time'] for arc in arcos_activos])+ (len(arcos_activos)-1)*10, 60)
    #chargeTime = mdl.objective_value -driveTime

    print(f'Total Route Time: {int(TotTime[0])} h {int(TotTime[1])} min')
    print(f'Drive Time: {int(driveTime[0])} h {int(driveTime[1])} min')
    print(f'Charging Time: {int(chargeTime[0])} h {int(chargeTime[1])} min')
        
    #plot in map
    #get final route
    api_key = '5b3ce3597851110001cf62487d4fb586fbde48c4a1920afb15fe4220' #Provide your personal API key
    clnt = client.Client(key=api_key) 

    route = ors.directions.directions(clnt, coordinates=[origin, *stations_sol, destination], 
                                      profile = 'driving-car', instructions=False)
    
    geom = route['routes'][0]['geometry']

    route_profile = pd.DataFrame(ors.convert.decode_polyline(geom,is3d=False)['coordinates'], 
                                 columns=['long','lat'])

    line = route_profile[['lat','long']]

    m = folium.Map(location=line.iloc[len(line)//2], zoom_start=7)

    start = origin[::-1] # inverts the position of lon, lat to lat, lon required by folium
    end = destination[::-1]

    folium.vector_layers.PolyLine(locations=line).add_to(m)
    folium.Marker(start,popup="Start", icon = folium.Icon(icon="glyphicon-map-marker",color='red')).add_to(m)
    folium.Marker(end,popup="End", icon = folium.Icon(icon="glyphicon-flag",color='green')).add_to(m)

    for station in stations_sol:
        folium.Marker(station[::-1],popup="{}\n{} kW".format(stations[station]['type'],stations[station]['power']), 
                      icon = folium.Icon(icon="glyphicon-flash",color='blue')).add_to(m)
    
    return m 

from ortools.linear_solver import pywraplp

def solveEVRP_ortools(arcs, stations, auto, soc_ini, soc_min, soc_end, overhead):

#    overhead = 10 # mins
    arcs = arcs.copy()
    nodes =  arcs['nodes']
    origin = 0
    destination = len(nodes)-1

    del arcs['nodes']
    
    Q = auto.nettoKwh[0]
    
    # big M to define indicator constraints
    M = 1000
    
    #initiate model
    model = pywraplp.Solver('EVRP', pywraplp.Solver.CBC_MIXED_INTEGER_PROGRAMMING)
    model.EnableOutput()
    # set a time limit to get a solution in miliseconds
    model.set_time_limit(30000)

    # define variables
    x = {arc:model.BoolVar('x[{}]'.format(arc)) for arc in arcs} 
    q = {arc:model.NumVar(0, Q, 'q[{}]'.format(arc)) for arc in arcs} 

    c_in = {i:model.NumVar(soc_min*Q, Q, 'c_in[{}]'.format(i)) for i in range(1,len(nodes))} 

    c_out = {i:model.NumVar(soc_min*Q, Q, 'c_out[{}]'.format(i)) for i in range(len(nodes)-1)} 

    t_in = {i:model.NumVar(0, 500, 't_in[{}]'.format(i)) for i in stations} 

    t_out = {i:model.NumVar(0, 500, 't_out[{}]'.format(i)) for i in stations} 

    # objective function
    model.Minimize(model.Sum(arcs[arc]['time']*x[arc] for arc in arcs)
                   + overhead*(model.Sum(x[arc] for arc in arcs) -1)
                   + model.Sum((t_out[i]-t_in[i]) for i in stations)
    #               + model.Sum((t_out[i]-t_in[i]) for (i,j) in x if x[(i,j)] == 1 and i!=0)
                  )
    
    # define constraints

    # consumption indicator constraint with big M formulation
    for arc in arcs:
        model.Add(q[arc] >= arcs[arc]['consumption'] - (1-x[arc])*M, 'C_{}'.format(arc))

    # exit flow 
    model.Add(model.Sum([x[0,j] for j in nodes if j !=0 and (0,j) in arcs]) == 1, 'flow0' )

    # continuous flow
    for i in stations:
        model.Add(model.Sum(x[(j,i)] for j in nodes if i!=j and (j,i) in arcs) 
                  == model.Sum(x[(i,j)] for j in nodes if i!=j and (i,j) in arcs), 'flow{}'.format(i)
                 )
    # how much to charge constraint
    for (i,j) in arcs:
        model.Add((c_out[i] - c_in[j]) == q[(i,j)],'')

    # initial and final battery level
    model.Add(c_out[origin] == soc_ini*Q, '')
    model.Add(c_in[destination] == soc_end*Q, '')

    # define piecewise functions for charger speeds
    pwf_ortools_params_dic = {50: [[  0.,  29.84061854,  79.00130959,  91.73437364,100.],
                                   [ 0., 15.46504219, 34.32467859, 43.45117686, 68.69722641]],
                              100: [[  0.,  22.52879574,  39.25771647,  40.58786094,100.],
                                    [ 0., 20.76008139, 25.63730618, 26.18398679, 42.55716897]],
                              150: [[  0.,  32.47273317,  75.39059379,  75.54804826,100.],
                                    [ 0., 11.57100678, 23.0430166 , 22.76026374, 34.99621425]],
                              350: [[  0.,  31.32698374,  78.54271751,  79.90956141,100.],
                                    [ 0.,  9.87999129, 23.30983567, 25.62739583, 34.1460323 ]]}

    curves = {i:{} for i in stations}

    def Piecewise1(s, params):

        bp_x = params[0]
        bp_y = params[1]

        n = len(bp_x)

        #variables
        curves[s]['z_in'] = {i:model.BoolVar('z_in_{}[{}]'.format(i,s)) for i in range(n)}
        curves[s]['s_in'] = {i:model.NumVar(0,1,'s_in_{}[{}]'.format(i,s)) for i in range(n)}

        curves[s]['z_out'] = {i:model.BoolVar('z_out_{}[{}]'.format(i,s)) for i in range(n)}
        curves[s]['s_out'] = {i:model.NumVar(0,1,'s_out_{}[{}]'.format(i,s)) for i in range(n)}

        #define SOS2 constraints
        for i in curves[s]['s_in']:
            model.Add(curves[s]['s_in'][i]<=curves[s]['z_in'][i],'')
            model.Add(curves[s]['s_out'][i]<=curves[s]['z_out'][i],'')
        model.Add(model.Sum(curves[s]['s_in'][i] for i in curves[s]['s_in']) == 1,'')
        model.Add(model.Sum(curves[s]['z_in'][i] for i in curves[s]['z_in']) <= 2,'')
        model.Add(model.Sum(curves[s]['s_out'][i] for i in curves[s]['s_out']) == 1,'')
        model.Add(model.Sum(curves[s]['z_out'][i] for i in curves[s]['z_out']) <= 2,'')
        for i in curves[s]['z_in']:
            for j in curves[s]['z_in']:
                if i!=j and j!= i+1 and i<j:
                    model.Add(curves[s]['z_in'][i]+curves[s]['z_in'][j]<=1,'')
                    model.Add(curves[s]['z_out'][i]+curves[s]['z_out'][j]<=1,'')

        model.Add(model.Sum(bp_x[i]*curves[s]['s_in'][i] for i in curves[s]['s_in']) == c_in[s]*100/Q,'')
        model.Add(model.Sum(bp_y[i]*curves[s]['s_in'][i] for i in curves[s]['s_in']) == t_in[s],'')
        model.Add(model.Sum(bp_x[i]*curves[s]['s_out'][i] for i in curves[s]['s_out']) == c_out[s]*100/Q,'')
        model.Add(model.Sum(bp_y[i]*curves[s]['s_out'][i] for i in curves[s]['s_out']) == t_out[s],'')

    for i in stations:
        Piecewise1(i,pwf_ortools_params_dic[stations[i]['power']])

    for i in stations:
        model.Add(t_out[i] >= t_in[i])

    # Solve EVRP
    solution = model.Solve()
    solution_type = {0:'Optimal', 1:'Feasible', 2: 'Infeasible', 6: 'Not Solved'}
    print('Solution: {}'.format(solution_type[solution]))
    
    # Print solution
    active_arcs = [arc for arc in x if x[arc].SolutionValue()!=0]
    active_nodes = list(set(np.array([ arc for arc in active_arcs]).flatten()))
    active_nodes.sort()

    chargeTimeAll = divmod(sum(t_out[i].SolutionValue()-t_in[i].SolutionValue() for i in stations),60)
    chargeTime = divmod(sum(t_out[i].SolutionValue()-t_in[i].SolutionValue() for i in active_nodes[1:-1]), 60)
    driveTime = divmod(sum([arcs[arc]['time'] for arc in active_arcs])+ (len(active_arcs)-1)*10, 60)
    routeTime = sum(t_out[i].SolutionValue()-t_in[i].SolutionValue() for i in active_nodes[1:-1]) \
                + sum([arcs[arc]['time'] for arc in active_arcs])+ (len(active_arcs)-1)*10

    stops = len(active_nodes)-2
    
    return routeTime, chargeTime, stops

hamburg = (10.01534, 53.57532)
dresden = (13.737262,51.050407)
munich = (11.576124, 48.137154)
frankfurt = (8.682127, 50.110924)
duesseldorf = (6.783333, 51.233334)
nuernberg = (11.061859, 49.460983)

#du_nu = getRouteData(origin=duesseldorf,destination=nuernberg, nr_stations=57)
#dd_fr = getRouteData(origin=dresden,destination=frankfurt, nr_stations=57)
#dd_mu = getRouteData(origin=dresden,destination=munich, nr_stations=57)
#hh_dd = getRouteData(origin=hamburg,destination=dresden, nr_stations=57)    
    
#routes = [dd_mu, hh_dd, dd_fr, du_nu]

start =[hamburg, dresden, dresden,duesseldorf]
end=[dresden, munich, frankfurt, nuernberg]

dtimes = []
ctimes = []
stops = []
oh = []
r = []

for n in range(10):
    for i in range(len(start)):
        route = getRouteData(origin=start[i],destination=end[i], nr_stations=57)
        
        for overhead in range(0,65,5):
            print('Route {}, Overhead {}, Attempt {}'.format(i,overhead,n+1))
            dtime, ctime, stop = solveEVRP_ortools(route[0],route[1], route[2], soc_ini=1, soc_min=.1, soc_end=.1, overhead=overhead)
            dtimes.append(dtime)
            ctimes.append(ctime)
            stops.append(stop)
            oh.append(overhead)
            r.append(i)
    
rep = pd.DataFrame({'DriveTime':dtimes,'ChargeTime':ctimes, 'stops': stops, 'overhead':oh, 'route': r})

#rep.set_index(['overhead', 'route'])
