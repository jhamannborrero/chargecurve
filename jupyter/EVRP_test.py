#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 20 10:13:15 2020

@author: jehamann
"""

import numpy as np
import matplotlib.pyplot as plt

from ortools.constraint_solver import pywrapcp
from ortools.constraint_solver import routing_enums_pb2

rnd = np.random

n_cs = 30 # number of charge stations
cs = [n+1 for n in range(n_cs)] #cs vertices
vertices = [0]+cs
destination = len(vertices)
vertices.append(destination) # append destination vertex to vertices

Q = 28 # EV Battery capacity
EC = 0.15 # Energy consumption kWh/km
speed = 110 # km/h

X = 500 # total route length in km

# Create coordinates:
rnd = np.random
rnd.seed(42)
loc_x = np.sort([0]+[rnd.randint(0,X)+rnd.rand()*5 for n in range(n_cs)]+[X]).round(1)
loc_y = np.round([rnd.rand()*5*(1-2*rnd.randint(0,2)) for n in range(len(vertices))])

def create_data_model(vehicles, mode):
    data = {}
    
    if mode == 1:
        data['distance_matrix'] =[[np.hypot((loc_x[i]-loc_x[j]),(loc_y[i]-loc_y[j])) for i in vertices] 
                     for j in vertices] 
    elif mode == 2:
        data['distance_matrix'] =[[np.hypot((loc_x[i]-loc_x[j]),(loc_y[i]-loc_y[j])) if i>j else 0 
                                   for i in vertices]                   
                                  for j in vertices] 
    data['times'] = np.array(data['distance_matrix'])*60/speed
    data['num_vehicles'] = vehicles
    data['depot'] = 0
    data['vehicle_capacities'] = [15 for n in range(vehicles)]
    rnd.seed(42)
    data['demands'] = [0]+[rnd.randint(1,10) for n in range(len(vertices)-2)]+[0]
    data['start'] = [0 for n in range(vehicles)]
    data['end'] = [len(vertices)-1 for n in range(vehicles)]
    return data   

def print_solution(data, manager, routing, assignment):
    """Prints assignment on console."""
    # Display dropped nodes.
    dropped_nodes = 'Dropped nodes:'
    for node in range(routing.Size()):
        if routing.IsStart(node) or routing.IsEnd(node):
            continue
        if assignment.Value(routing.NextVar(node)) == node:
            dropped_nodes += ' {}'.format(manager.IndexToNode(node))
    print(dropped_nodes)
    # Display routes
    total_time = 0
    total_load = 0
    for vehicle_id in range(data['num_vehicles']):
        index = routing.Start(vehicle_id)
        plan_output = 'Route for vehicle {}:\n'.format(vehicle_id)
        route_time = 0
        route_load = 0
        while not routing.IsEnd(index):
            node_index = manager.IndexToNode(index)
            route_load += data['demands'][node_index]
            plan_output += ' {0} Load({1}) {2}min -> '.format(node_index, route_load, route_time)
            previous_index = index
            index = assignment.Value(routing.NextVar(index))
            route_time += routing.GetArcCostForVehicle(
                previous_index, index, vehicle_id)
        plan_output += ' {0} Load({1})\n'.format(manager.IndexToNode(index),
                                                 route_load)
        plan_output += 'Time of the route: {}min\n'.format(route_time)
        plan_output += 'Load of the route: {}\n'.format(route_load)
        print(plan_output)
        total_time += route_time
        total_load += route_load
    print('Total Time of all routes: {}min'.format(total_time))
    print('Total Load of all routes: {}'.format(total_load))

def plot_solution(data, manager, routing, solution):
    route_idx =[]
    for vehicle_id in range(data['num_vehicles']):
        index = routing.Start(vehicle_id)
        r_idx =  []
        while not routing.IsEnd(index):
            node_index = manager.IndexToNode(index)
            index = solution.Value(routing.NextVar(index))
            r_idx.append(node_index)
        r_idx.append(routing.Size())
        route_idx.append(r_idx)
        
    plt.figure(figsize=(12,5))
    plt.scatter(loc_x,loc_y,color="green")
    
    for i, route in enumerate(route_idx):
        plt.plot(loc_x[route],loc_y[route], alpha=0.3, label='vehicle {}'.format(i))

    for i in cs:
        plt.annotate('$q_{}={}$'.format(i,data['demands'][i]),(loc_x[i]+1,loc_y[i]+0.1), fontsize='x-large')

    plt.plot(loc_x[0],loc_y[0],color='red',marker='s')
    plt.annotate('Start', (loc_x[0]+5, loc_y[0]), fontsize='x-large')
    plt.plot(loc_x[-1], loc_y[-1], color='blue', marker='s')
    plt.annotate('Dest', (loc_x[-1]+5, loc_y[-1]), fontsize='x-large')
    plt.legend()

    plt.xlabel("Distance x [km]", fontsize='x-large')
    plt.xlabel("Distance y [km]", fontsize='x-large')
    plt.title("Routes | OVRP")
    
def main(vehicles, mode):
    """Solve the CVRP problem."""
    # Instantiate the data problem.
    data = create_data_model(vehicles, mode)

    # Create the routing index manager.
    manager = pywrapcp.RoutingIndexManager(len(data['distance_matrix']),
                                           data['num_vehicles'], data['start'], data['end'])

    # Create Routing Model.
    routing = pywrapcp.RoutingModel(manager)


    # Create and register a transit callback.
    def distance_callback(from_index, to_index):
        """Returns the distance between the two nodes."""
        # Convert from routing variable Index to distance matrix NodeIndex.
        from_node = manager.IndexToNode(from_index)
        to_node = manager.IndexToNode(to_index)
        return data['distance_matrix'][from_node][to_node]

    # Create and register a transit callback.
    def time_callback(from_index, to_index):
        """Returns the distance between the two nodes."""
        # Convert from routing variable Index to distance matrix NodeIndex.
        from_node = manager.IndexToNode(from_index)
        to_node = manager.IndexToNode(to_index)
        return data['times'][from_node][to_node]
    
    transit_callback_index = routing.RegisterTransitCallback(time_callback)

    # Define cost of each arc.
    routing.SetArcCostEvaluatorOfAllVehicles(transit_callback_index)


    # Add Capacity constraint.
    def demand_callback(from_index):
        """Returns the demand of the node."""
        # Convert from routing variable Index to demands NodeIndex.
        from_node = manager.IndexToNode(from_index)
        return data['demands'][from_node]

    demand_callback_index = routing.RegisterUnaryTransitCallback(
        demand_callback)
    routing.AddDimensionWithVehicleCapacity(
        demand_callback_index,
        0,  # null capacity slack
        data['vehicle_capacities'],  # vehicle maximum capacities
        True,  # start cumul to zero
        'Capacity')
    # Allow to drop nodes.
    penalty = 100
    for node in range(1, len(data['distance_matrix'])-1):
        routing.AddDisjunction([manager.NodeToIndex(node)], penalty)

    # Setting first solution heuristic.
    search_parameters = pywrapcp.DefaultRoutingSearchParameters()
    search_parameters.first_solution_strategy = (
        routing_enums_pb2.FirstSolutionStrategy.PATH_CHEAPEST_ARC)

    # Solve the problem.
    assignment = routing.SolveWithParameters(search_parameters)

    # Print solution on console.
    if assignment:
        print_solution(data, manager, routing, assignment)
        plot_solution(data, manager, routing, assignment)
    else:
        print('No solution')
        
main(vehicles=1, mode=1)