import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import sys, warnings
sys.path.append('../chargecurve/')
warnings.filterwarnings('ignore')
from databases import load_base2_ftp, query_station
from functions import get_peaks, collect_charges, getArcEnergy
from chargecurve import AutoCharge, ChargingCurve
import datetime
import pwlf
import openrouteservice as ors
from openrouteservice import client
import folium 
import geopy.distance 
import psycopg2

#1 get route



api_key = '5b3ce3597851110001cf62487d4fb586fbde48c4a1920afb15fe4220' #Provide 
your personal API key
clnt = client.Client(key=api_key) 

hamburg = [10.01534, 53.57532]
munich = [11.57549,48.13743]
dresden = [13.737262,51.050407]


def getRoute(origin, destination):
    # get route from openrouteservice
    route = ors.directions.directions(clnt, coordinates=[origin, destination], 
profile = 'driving-car',
                                      instructions=False, elevation=True)
    return route

route = getRoute(hamburg, dresden)

geom = route['routes'][0]['geometry']

# extracts lat, lon and altitude
route_profile = 
pd.DataFrame(ors.convert.decode_polyline(geom,is3d=True)['coordinates'], 
                             columns=['long','lat','alt'])

# total route distance
route_dist = round(route['routes'][0]['summary']['distance'])

#plot route on map
DE = [51.133481, 10.018343]

m = folium.Map(location=DE, zoom_start=6)

line = route_profile[['lat','long']]
start = hamburg[::-1] # inverts the position of lon, lat to lat, lon required by 
folium
end = dresden[::-1]

folium.vector_layers.PolyLine(locations=line).add_to(m)
folium.Marker(start,popup="Start", icon = 
folium.Icon(icon="glyphicon-map-marker",color='red')).add_to(m)
folium.Marker(end,popup="End", icon = 
folium.Icon(icon="glyphicon-flag",color='green')).add_to(m)

m

# 2 Get stations along the route

step = 10
steped = route_profile[::step]


def query_station(lat, lon, kw):
    """
    Query the ChargeStations from 'stations' DB based on the given latitude and 
longitude information.
    Returns a pandas DataFrame with charge point information located at the 
given lat, lon location.
    lat (float): latitude
    lon (float): longitude
    """
    
    #PostgreSQL connection (Charge Station data)
    pg_host = "10.19.160.198"
    pg_port = "5432"
    pg_user = "stations-api"
    pg_password = "Zj9XkLMu8BCJ3_sK7WGYqhD"
    database = "stations"

    con = psycopg2.connect(database=database, user=pg_user, 
password=pg_password, host=pg_host, port=pg_port)

    delta = 0.01   # +/-delta range for the latitude/longitude
    
    # query the stations DB for the location of the charge station at charging 
time
    
    def query(lat, lon, delta, kw):
        s = "SELECT cs.longitude, cs.latitude, cs.\"addressStreet\", cs.id AS 
chargestation_id, cp.id AS chargepoint_id, cp.kw, cp.ampere, cp.volt, cp.type "\
        "FROM \"chargepoints\" AS cp LEFT JOIN \"chargegroups\" AS cg ON cg.id = 
cp.chargegroup_id "\
        "LEFT JOIN \"chargestations\" AS cs ON cs.id = cg.chargestation_id "\
        "WHERE latitude BETWEEN {} AND {} AND longitude BETWEEN {} AND {} AND kw 
>= {};".format(lat-delta, lat+delta,lon-delta, lon+delta,kw)
        
        return s
    cs = pd.concat(pd.read_sql_query(query(lat.values[n], lon.values[n], delta, 
kw),con) for n in range(len(lat)))
    con.close()
    cs = cs.drop_duplicates(subset='chargestation_id')

    return cs



_ = query_station(steped.lat, steped.long, kw=40)

# sort stations distance from origin
_['dist_to_origin'] = [geopy.distance.distance(x, hamburg[::-1]).km*100 for x in 
list(zip(_.latitude, _.longitude))]
_ = _.sort_values(by='dist_to_origin')
_

_ = _[(_.type == 'CCS')]# | (_.type == 'TYP2')]
_


m = folium.Map(location=DE, zoom_start=6)

line = route_profile[['lat','long']]
start = hamburg[::-1] # inverts the position of lon, lat to lat, lon required by 
folium
end = dresden[::-1]

folium.vector_layers.PolyLine(locations=line).add_to(m)
folium.Marker(start,popup="Start", icon = 
folium.Icon(icon="glyphicon-map-marker",color='red')).add_to(m)
folium.Marker(end,popup="End", icon = 
folium.Icon(icon="glyphicon-flag",color='green')).add_to(m)

for station in range(len(_)):
    folium.Marker((_.latitude.iloc[station],_.longitude.iloc[station]),popup="{} 
{}kW".format(_.type.iloc[station],_.kw.iloc[station]), icon = 
folium.Icon(icon="glyphicon-flash",color='blue')).add_to(m)
m

# 3  define nodes and get distances and elevation

stations = list(zip(_.longitude,_.latitude ))
nodes = [tuple(hamburg), *stations, tuple(dresden)]

# matrix limit to 59 locations
matrix = ors.distance_matrix.distance_matrix(clnt, locations=nodes, profile = 
'driving-car',
                                             metrics=['distance'])
matrix = matrix['distances']

elevation = ors.elevation.elevation_line(clnt, format_in='polyline', 
geometry=nodes, format_out='polyline')
elevation = elevation['geometry']

# 4  Calculate energy for arcs

arcs = [(nodei,nodej) for i, nodei in enumerate(nodes) for j, nodej in 
enumerate(nodes) if i!=j and i<j]

distances = {(nodei,nodej):matrix[i][j] for i, nodei in enumerate(nodes) 
             for j, nodej in enumerate(nodes) if i!=j and i<j}

speed =110 #km/h

times = {arc:round(distances[arc]*60/(1000*speed),1) for arc in arcs}

auto = query_vehicle('IONIQ Elektro')
add_weight = 0
t_in = 20
t_out = 15


energies = {(nodei,nodej):getArcEnergy(auto, add_weight, 
                                       distance = matrix[i][j], t_in, t_out, 
                                       h_start=elevation[i][2], 
                                       h_end=elevation[j][2], 
                                       speed=speed, 
                                       wind=0, wet=False)
            for i, nodei in enumerate(nodes) 
            for j, nodej in enumerate(nodes) if i!=j and i<j}

# 5  get charge curves


hyundai = load_base2_ftp('ecu_bcddc2d07024')
hyundai = hyundai[hyundai.model == 'HYUNDAIIONIQ']

#select while charging data, for Hyundai the conditions are as below
mask = (hyundai.hvBatteryCurrent < 0) & (hyundai.rpmEmotor <=0) \
& (hyundai.timestamp > datetime.datetime(2019,12,1))

#find peaks to extract charging curves
peaks_id = get_peaks(hyundai[mask], 'hvSocActualDisplay')

# collect charging data as AutoCharge objects
hyundai_charges = collect_charges(hyundai[mask], peaks_id)

# getcharging curves
HyundaiCharges = ChargingCurve(hyundai_charges)
#HyundaiCharges.data.chargepoint_id.unique()


def getMedianChargeCurve(data, temp = False):
        """
        Function to determine the median value at a constant SoC, by aggregating
        the power values in 1 % SoC bins.
        
        Returns a panda DataFrame with median values of Power and Charging time 
in minutes
        at 1 % bins.
        """
        cols = ['soc_display','power']
        bins = pd.IntervalIndex.from_tuples([(i, i+1)  for i in range(100)])
        
        if temp:
            mask = (data.BatteryTemp == temp)
            dff = data[cols][mask]
        else:
            dff = data[cols]
            
        dff['ranges'] = pd.cut(dff['soc_display'], bins = bins)
    
        medians = dff.groupby('ranges').median()
        medians['remainingTime'] = 
battCapacity*(1-medians['soc_display'][1:]/100)/medians['power'][1:]
        medians['elapsedTime'] = 
abs((medians['remainingTime']*60).diff(1)).cumsum()
        medians['elapsedTime'] = medians['elapsedTime'] - 
medians['elapsedTime'].min()
        medians=medians.dropna()

        return medians
    
battCapacity = 28         
curve50kW = HyundaiCharges.ChargePowerCurve(50,battCapacity=28)    

_ = getMedianChargeCurve(curve50kW)

plt.plot(_.soc_display, _.elapsedTime, 'o')
plt.plot(_.soc_display, _.power, 'o')


# select charging curve for CP = 95195
#power = HyundaiCharges.ChargePowerCurve(50.0, battCapacity=28)
cp = HyundaiCharges.ChargePointCurve(95195, battCapacity=28)

Y = _.elapsedTime.values
X = _.soc_display.values

Y1 = cp.dropna().elapsedTime.values
X1 = cp.dropna().soc_display.values

# initialize piecewise linear fit with your x and y data
my_pwlf = pwlf.PiecewiseLinFit(X, Y)
#my_pwlf1 = pwlf.PiecewiseLinFit(X1, Y1)

# fit the data for 5 line segments
res = my_pwlf.fit(5)
#res1 = my_pwlf1.fit(5)

# predict for the determined points
xHat = np.linspace(min(X), max(X), num=len(X))
yHat = my_pwlf.predict(xHat)

#xHat1 = np.linspace(min(X1), max(X1), num=len(X1))
#yHat1 = my_pwlf.predict(xHat1)

# plot the results
plt.figure(figsize=(10,7))
plt.plot(X, Y, 'o', alpha =0.3, label = 'Power')
#plt.plot(X1, Y1, 'o', alpha =0.3, label = 'CP')
plt.plot(xHat, yHat, c='red', label = 'Piecewise Function Power')
#plt.plot(xHat1, yHat1, c='red', label = 'Piecewise Function CP')
plt.plot(my_pwlf.fit_breaks,my_pwlf.predict(my_pwlf.fit_breaks), 'o',  c='red', 
label = 'Break points Power')
#plt.plot(my_pwlf1.fit_breaks,my_pwlf1.predict(my_pwlf1.fit_breaks), 'o',  
c='red', label = 'Break points CP')
plt.ylabel('Time [min]', fontsize='x-large')
plt.xlabel('SoC [%]', fontsize='x-large')
plt.legend(fontsize='x-large')
plt.tight_layout()

Y = _.elapsedTime.values
X = _.soc_display.values

def fitPiecewise(X, Y, segments=5):
    my_pwf = pwlf.PiecewiseLinFit(X, Y)
    res = my_pwf.fit(segments)
    breakPoints = 
[(my_pwf.fit_breaks[n],my_pwf.predict(my_pwlf.fit_breaks[n])[0]) for n in 
range(len(my_pwf.fit_breaks))]
    cplex_params = [my_pwf.slopes[0]] + [breakPoints] + [my_pwf.slopes[-1]]

    return cplex_params

print(fitPiecewise(X,Y))   

#6  Solve with cplex

def getGeoDistance(line):
    """
    Calculates the distance between two points based on its latitude and 
longitude
    
    Parameter:
    ----------
    line : Route Profile (pandas DataFrame) with lat and long info
    
    Returns:
    --------
    two lists, one with the distance between each point and the other with the 
    cumulative sum of the distances.
    """
    
    x0 = list(zip(line['lat'], line['long']))
    x1 = x0[1:]
    s = [0]
    for i in range(len(x1)):
        s.append(geopy.distance.distance(x0[i],x1[i]).km)
    
    return s, np.cumsum(s)

def getRouteProfile(route):
    geom = route['routes'][0]['geometry']
    
    # extracts lat, lon and altitude
    route_profile = 
pd.DataFrame(ors.convert.decode_polyline(geom,is3d=True)['coordinates'], 
                                 columns=['long','lat','alt'])
    geodist = getGeoDistance(route_profile)
    
    route_profile['s'] = geodist[1]
    route_profile['ds'] = geodist[0]
    
    return route_profile

def getArcHeights(route):
    
    geom = route['routes'][0]['geometry']
    
    # extracts lat, lon and altitude
    route_profile = 
pd.DataFrame(ors.convert.decode_polyline(geom,is3d=True)['coordinates'], 
                                 columns=['long','lat','alt'])
    geodist = getGeoDistance(route_profile)
    
    h_i = route_profile.alt.iloc[0]
    h_f = route_profile.alt.iloc[-1]
    
    return (h_i, h_f)

arcs = {(loci,locj): getRoute(loci,locj) for i, loci in enumerate(nodes) for j, 
locj in enumerate(nodes) if i!=j and i<j}
arcs

profiles = {arc: getRouteProfile(arcs[arc]) for arc in arcs }
profiles

hights = {arc: getArcHeights(arcs[arc]) for arc in arcs }


def getArcRoute(arc):
    # get route from openrouteservice
    route = ors.directions.directions(clnt, coordinates=arc, profile = 
'driving-car',
                                      instructions=False, elevation=True)
    return route

distances = {arc: arcs[arc]['routes'][0]['summary']['distance'] for arc in arcs}
distances



